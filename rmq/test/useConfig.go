package main

const (
	COMMON_EXCHANGE    = "common_exchange"
	COMMON_QUEUE       = "common_queue"
	COMMON_ROUTING_KEY = "common_routing_key"

	//延迟
	COMMON_DELAY_EXCHANGE    = "common_delay_exchange"
	COMMON_DELAY_QUEUE       = "common_delay_queue"
	COMMON_DELAY_ROUTING_KEY = "common_delay_routing_key"
	//广播
	COMMON_FANOUT_EXCHANGE = "common_fanout_exchange"
	//主题
	COMMON_TOPIC_EXCHANGE = "common_topic_exchange"

	//事件
	EVENT_PAY   = "order_pay"   //订单支付事件
	EVENT_UNPAY = "order_unpay" //订单未支付

	//队列
	QUEUE_ORDER_EVENT_LISTENER = "queue_order_event_listener"
)

var COMMON_FANOUT_QUEUE = []string{
	"common_fanout_queue",
	"common_fanout_queue2",
}

type queueDefine []map[string]interface{}

var QUEUE_DEFINE = map[string]queueDefine{
	QUEUE_ORDER_EVENT_LISTENER: {
		{
			"topic": []string{
				EVENT_PAY,
				EVENT_UNPAY,
			},
			"callback": []string{"callback"},
		},
	},
}

//定义一个关联配置
var Callback = map[string]interface{}{
	"test": &QueueCall{},
}

//延迟队列回调关联处理
var CallbackDelay = map[string]interface{}{
	"delay_order": &DelayOrder{},
}

var CallbackFanout = map[string]interface{}{
	"fanout": &FanoutCall{},
}
