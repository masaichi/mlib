package main

import (
	"flag"
	"gitee.com/masaichi/mlib/rmq"
	"gitee.com/masaichi/mlib/rmq/rmqconstant"
	"gitee.com/masaichi/mlib/rmq/rmqlib"
	"gitee.com/masaichi/mlib/rmq/rmqstruct"
	"log"
)

type test struct {
	Id   int
	Name string
}

func main() {
	var c *string
	c = flag.String("c", "", "choose one")
	flag.Parse()
	if *c == "" {
		log.Fatal("c参数一定要传递")
	}
	//初始化连接
	rmqlib.InitConn("", "", "", 5672)
	if *c == "common" {
		//普通消费队列
		testQueue()
	} else if *c == "delay" {
		//延迟队列
		testDelayQueue()
	} else if *c == "fanout" {
		//公共广播队列
		testCommonFanoutQueue()
	}
	//需要实现的效果
	//testQueue() //入队列
	//rmq.intoDelayQueue() //入延迟队列
	//rmq.broadcast()      //广播
	//1. 入普通队列
	//testQueue()
	//2. 入延迟队列
	//testDelayQueue()
	//3. 广播
	//testCommonFanoutQueue()
	//4. 消息公共队列
	//testCommonConsume()
}

//入消息队列
func testQueue() {
	tt := []interface{}{}
	tt = append(tt, test{
		Id:   1,
		Name: "123",
	})
	param := rmqstruct.NewQueueParam(1, tt, []string{"test", "Show"})
	rmq.IntoQueue(*param)
}

//进入延迟消息队列
func testDelayQueue() {
	tt := []interface{}{}
	tt = append(tt, test{
		Id:   2,
		Name: "123",
	})
	param := rmqstruct.NewQueueParam(1, tt, []string{"delay_order", "Show"})
	rmq.IntoDelayQueue(*param, 2*1000)
}

func testCommonFanoutQueue() {
	tt := []interface{}{}
	tt = append(tt, test{
		Id:   2,
		Name: "广播",
	})
	param := rmqstruct.NewQueueBroadcastParam(rmqconstant.EVENT_UNPAY, tt, []string{"fanout", "Show"})
	rmq.IntoCommonFanoutQueue(*param)
}
