package main

import (
	"flag"
	"gitee.com/masaichi/mlib/rmq"
	"gitee.com/masaichi/mlib/rmq/rmqlib"
	"log"
)

func main() {
	log.Println("%v", CallbackDelay)

	var c *string
	c = flag.String("c", "", "消费者")
	flag.Parse()
	if *c == "" {
		log.Fatalln("请填写参数c")
	}
	//初始化连接
	rmqlib.InitConn("", "", "", 5672)
	if *c == "common" {
		//普通队列消费
		testCommonConsume()
	} else if *c == "delay" {
		//延迟队列消费
		testCommonDelayConsume()
	} else if *c == "fanout" {
		//广播队列消费
		testCommonFanoutConsume()
	}
}

//普通队列消费
func testCommonConsume() {
	rmq.CommonQueueConsume(Callback)
}

//延时队列消费
func testCommonDelayConsume() {
	rmq.CommonDelayQueueConsume(CallbackDelay)
}

//广播队列消费
func testCommonFanoutConsume() {
	rmq.CommonFanoutQueueConsume(CallbackFanout)
}
