package main

import (
	"encoding/json"
	"fmt"
	"gitee.com/masaichi/mlib/rmq/rmqstruct"
)

//以下这些是在实际业务中
type QueueCall struct {
}

type DelayOrder struct {
}

type FanoutCall struct {
}

func (this *QueueCall) Show(str string) {
	fmt.Printf("test%s", str)
}

func (this *DelayOrder) Show() {
	fmt.Println("这是delay order的show方法")
}

func (this *FanoutCall) Show(str string) {
	fmt.Println("这是广播队列的show方法")
	item := rmqstruct.NewEmptyQueueParamItem()
	json.Unmarshal([]byte(str), item)
	fmt.Printf("%v", item)
}
