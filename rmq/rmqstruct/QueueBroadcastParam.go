package rmqstruct

type QueueBroadcastParam struct {
	Event    string //广播事件
	Data     []interface{}
	Callback []string
}

type queueBroadcastParamItem struct {
	Event    string
	Data     interface{}
	Callback []string
}

func NewQueueBroadcastParam(event string, data []interface{}, callback []string) *QueueBroadcastParam {
	return &QueueBroadcastParam{Event: event, Data: data, Callback: callback}
}

func NewQueueBroadcastParamItem(event string, data interface{}, callback []string) *queueBroadcastParamItem {
	return &queueBroadcastParamItem{Event: event, Data: data, Callback: callback}
}
