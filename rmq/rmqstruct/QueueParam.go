package rmqstruct

type QueueParam struct {
	Service  int
	Data     []interface{}
	Callback []string
}

type QueueParamItem struct {
	Service  int
	Data     interface{}
	Callback []string
}

//获取一个实例
func NewQueueParam(service int, data []interface{}, callback []string) *QueueParam {
	return &QueueParam{
		Service:  service,
		Data:     data,
		Callback: callback,
	}
}

//队列实际的消息
func NewQueueParamItem(service int, data interface{}, callback []string) *QueueParamItem {
	return &QueueParamItem{
		Service:  service,
		Data:     data,
		Callback: callback,
	}
}

func NewEmptyQueueParamItem() *QueueParamItem {
	return &QueueParamItem{}
}
