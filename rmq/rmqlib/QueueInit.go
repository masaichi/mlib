package rmqlib

import (
	"fmt"
	"gitee.com/masaichi/mlib/rmq/rmqconstant"
)

//初始化通用队列
func CommonInit() error {
	mq := NewMQ()
	if mq == nil {
		fmt.Errorf("mq init error")
	}
	defer mq.Channel.Close()
	//声明交换机
	err := mq.Channel.ExchangeDeclare(rmqconstant.COMMON_EXCHANGE, "direct", false, false, false, false, nil)
	if err != nil {
		return fmt.Errorf("Exchange error", err)
	}
	//绑定交换机
	qs := fmt.Sprintf("%s", rmqconstant.COMMON_QUEUE)
	err = mq.DecQueueAndBind(qs, rmqconstant.COMMON_ROUTING_KEY, rmqconstant.COMMON_EXCHANGE)
	if err != nil {
		return fmt.Errorf("Queue Bind error", err)
	}
	return nil
}

//延迟队列
func CommonDelayInit() error {
	mq := NewMQ()
	if mq == nil {
		fmt.Errorf("delay mq init error")
	}
	defer mq.Channel.Close()
	//声明交换机
	err := mq.Channel.ExchangeDeclare(rmqconstant.COMMON_DELAY_EXCHANGE, "x-delayed-message", false, false, false, false, map[string]interface{}{"x-delayed-type": "direct"})
	if err != nil {
		return fmt.Errorf("DelayExchange error", err)
	}
	//绑定交换机
	qs := fmt.Sprintf("%s", rmqconstant.COMMON_DELAY_QUEUE)
	err = mq.DecQueueAndBind(qs, rmqconstant.COMMON_DELAY_ROUTING_KEY, rmqconstant.COMMON_DELAY_EXCHANGE)
	if err != nil {
		return fmt.Errorf("Queue Bind error", err)
	}
	return nil
}

//定义广播
func CommonFanoutInit() error {
	mq := NewMQ()
	if mq == nil {
		fmt.Errorf("fanout mq init error")
	}
	defer mq.Channel.Close()
	//声明交换机
	err := mq.Channel.ExchangeDeclare(rmqconstant.COMMON_FANOUT_EXCHANGE, "fanout", false, false, false, false, nil)
	if err != nil {
		return fmt.Errorf("FanoutExchange error", err)
	}
	//队列绑定交换机,消息都会发送到这些队列上
	//绑定交换机
	qs := fmt.Sprintf("%s", rmqconstant.COMMON_FANOUT_QUEUE)
	err = mq.DecQueueAndBind(qs, "", rmqconstant.COMMON_FANOUT_EXCHANGE)
	if err != nil {
		return fmt.Errorf("Fanout Queue Bind error", err)
	}
	//可以做成配置，切片的形式进行绑定,一个虚拟主机里只有一个消息队列名称
	//qs := ""
	//for _, one := range rmqconstant.COMMON_FANOUT_QUEUE {
	//	if qs != "" {
	//		qs = qs + ","
	//	}
	//	qs = qs + one
	//}
	//err = mq.DecQueueAndBind(qs, "", rmqconstant.COMMON_FANOUT_EXCHANGE)
	//if err != nil {
	//	return fmt.Errorf("Queue Bind error", err)
	//}
	return nil
}

//定义主题
func CommonTopicInit() error {
	mq := NewMQ()
	if mq == nil {
		fmt.Errorf("topic mq init error")
	}
	defer mq.Channel.Close()
	//声明交换机
	err := mq.Channel.ExchangeDeclare(rmqconstant.COMMON_TOPIC_EXCHANGE, "topic", false, false, false, false, nil)
	if err != nil {
		return fmt.Errorf("TopicExchange error", err)
	}
	//队列绑定交换机,消息都会发送到这些队列上
	//可以做成配置，切片的形式进行绑定,一个虚拟主机里只有一个消息队列名称
	qs := ""
	for _, one := range rmqconstant.COMMON_FANOUT_QUEUE_ARR {
		if qs != "" {
			qs = qs + ","
		}
		qs = qs + one
	}
	err = mq.DecQueueAndBind(qs, rmqconstant.COMMON_DELAY_ROUTING_KEY, rmqconstant.COMMON_FANOUT_EXCHANGE)
	if err != nil {
		return fmt.Errorf("Queue Bind error", err)
	}
	return nil
}

//思路
//todo 广播是通过定义不同的广播交换机，支付订单，未付款等等，然后绑定到指定的消息队列，关键因素有：1. 消息队列  2. 事件
//todo 如果支付，就将消息扔到支付交换机，交换机会广播到这个消息队列
