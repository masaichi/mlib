package rmqlib

import (
	"fmt"
	"github.com/streadway/amqp"
	"log"
)

var MQConn *amqp.Connection

func init() {
	//获取配置

	//dsh := fmt.Sprintf("amqp://%s:%s@%s:%d/", "masaichi", "85168908", "47.122.0.10", 5672)
	//conn, err := amqp.Dial(dsh)
	//if err != nil {
	//	log.Fatal(err)
	//}
	//MQConn = conn
	//log.Print(MQConn.Major)
}

func GetConn() *amqp.Connection {
	return MQConn
}

//初始化mq连接
func InitConn(user string, pwd string, host string, port int) {
	dsh := fmt.Sprintf("amqp://%s:%s@%s:%d/", user, pwd, host, port)
	conn, err := amqp.Dial(dsh)
	if err != nil {
		log.Fatal(err)
	}
	MQConn = conn
	log.Print(MQConn.Major)
}
