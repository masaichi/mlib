# mlib

#### 介绍
go工具库

#### 软件架构
> 目前主要是面向过程的写法

rmq 文件夹是主要的库  
rmqconstant 主要是存放定义好的一些常量，比如自带公共的队列，交换机，路由key的配置等  
rmqlib 对mq的封装  
rmqstruct 存放结构体  
RMQ 入队列的方法的封装  
RMQClient 消费的方法封装 

#### 安装教程

1.  go get -u gitee.com/masaicih/mlib

#### 使用说明
##### 一 生产端
1. 创建连接
   ```
   rmqlib.InitConn("用户名", "密码", "host", 端口)
   ```
2. 将消息入队列，已经默认配置一个交换机，以及一个延迟交换机
   ```
   callback-["绑定的结构体", "回调方法"]
    //入消息队列
    func testQueue() {
        tt := []interface{}{}
        tt = append(tt, test{
            Id:   1,
            Name: "123",
        })
        param := rmqstruct.NewQueueParam(1, tt, []string{"test", "Show"})
        rmq.IntoQueue(*param)
    }
    
    //进入延迟消息队列
    func testDelayQueue() {
        tt := []interface{}{}
        tt = append(tt, test{
            Id:   2,
            Name: "123",
        })
        param := rmqstruct.NewQueueParam(1, tt, []string{"delay_order", "Show"})
        rmq.IntoDelayQueue(*param, 2*1000)
    }
   ```

##### 二 消费端
1. 初始化连接
    ```
    rmqlib.InitConn("用户名", "密码", "host", 端口)
    ```
2. 定义一个回调关联的配置
   ```
    //定义一个关联配置
    var Callback = map[string]interface{}{
        "test": &QueueCall{},
    }
   ```
3. 从队列中获取消息，进行消费
   ```
      rmq.CommonQueueConsume(Callback)
   //callback作用是，在生产端会传递callback参数，包含test，和要回调的QueueCall方法
   //消费时，比如消息的callback为 test  Show,消费端消费时，会取传递过来的Callback中的test的结构体，调用QueueCall的Show方法
   //Show方法必须为暴露方法
   ```

##### 测试例子
##### 一 生产端
1. 在server.go中的InitConn方法中填写你的mq信息
2. 运行
```gitignore
 go run ./server.go -c common
```

##### 二 消费端
1. client.go中的InitConn方法中填写你的mq信息
```
//因为需要使用到useConfig service文件里的配置
 go run useConfig.go service.go client.go -c common
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
